# forelesning

En CLI for http://forelesning.gjovik.ntnu.no/ for å se forelesninger fra terminalen.
Krever vlc for å spille av forelesninger.


### Funksjonalitet

 - -s `ROMNR` Se stream fra valgt rom
 - -l `NAVN` Velg foreleser og få opp de siste forelesninger
 - -t `EMNE` Velg emne og få opp de siste forelesningene
 - -sl `NAVN` Søk etter foreleser
 - -st `EMNE` Søk etter emne
 - -pl Print alle forelesere
 - -pt Print alle emner
