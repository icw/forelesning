#!/usr/bin/python

import os
import argparse
import requests
import subprocess
import re
from bs4 import BeautifulSoup

#Input path to vlc:
VLCPATH = "/usr/bin/vlc"


VERBOSE = 0
STREAM = ""
ROOMS = "lille-eureka", "store-eureka", "smaragd1", "K102", "K105", "K113", "C007", "A146", "A153", "A154", "A254", "B210", "D101", "D201", "E211"
LIST_TOPIC = ""
LIST_LECTURER = ""
SEARCH_TOPIC = ""
SEARCH_LECTURER = ""
SELECT_LECTURER = ""
SELECT_TOPIC = ""
URLPUBLISH = 'https://forelesning.gjovik.ntnu.no/publish/'


parser = argparse.ArgumentParser(prog='forelesning.py')
parser.add_argument('-v','--verbose',dest="verbose",help='Turn verbosity on',default=False,action="store_true")
parser.add_argument('-s','--stream',dest="stream",help='Stream from either "lille-eureka", "store-eureka", "smaragd1", "K102", "K105", "K113", "C007", "A146", "A153", "A154", "A254", "B210", "D101", "D201", "E211"', metavar="NameOfRoom",default=STREAM)
parser.add_argument('-l','--select_lecturer',dest="select_lecturer", help='Select lecurer',metavar="NameOfLecturer", default=SELECT_LECTURER)
parser.add_argument('-t','--select_topic',dest="select_topic", help='Select topic',metavar="NameOfTopic", default=SELECT_TOPIC)
parser.add_argument('-sl','--search_lecturer',dest="search_lecturer", help='Search for a lecturer',metavar="NameOfLecturer", default=SEARCH_LECTURER)
parser.add_argument('-st','--search_topic',dest="search_topic", help='Search for a topic', metavar="NameOfTopic", default=SEARCH_TOPIC)
parser.add_argument('-pl','--print_lecturer',dest="list_lecturer", help='List all lecurers',default=False,action="store_true")
parser.add_argument('-pt','--print_topic',dest="list_topic", help='List all topics', default=False,action="store_true")


arguments = parser.parse_args()

VERBOSE = arguments.verbose
LIST_LECTURER = arguments.list_lecturer
LIST_TOPIC = arguments.list_topic
STREAM = str(arguments.stream)
SELECT_LECTURER = arguments.select_lecturer
SELECT_TOPIC = arguments.select_topic
SEARCH_LECTURER = arguments.search_lecturer
SEARCH_TOPIC = arguments.search_topic


def verbose(text):
    if VERBOSE:
        print (text)

def req(url):
    verbose("Fetching URL")
    resp = requests.get(url)
    if resp.status_code == 200:
        verbose("Success fetching URL")
    if resp.status_code != 200:
        verbose("An error has occured fetching the URL, error code is:")
        verbose(resp.status_code)
    soup = BeautifulSoup(resp.content, 'html.parser')
    return soup

def listPrint(line2, i):
     var = str(line2.text)
     formatting = var.splitlines()[5].replace('Audio Camera Screen Combined', '')
     print("ID:       ",i)
     print("Title:    ",var.splitlines()[4])
     print("Lecturer: ",var.splitlines()[3])
     print("Topic:    ",formatting)
     print("Duration: ",var.splitlines()[2])
     print("Date:     ",var.splitlines()[1])
     print("\n")

def objPrint(obj):
    for i, line2 in enumerate(reversed(obj)):
        listPrint(line2, i)

def objPrintSelect(obj, user):
    for i, line2 in enumerate(reversed(obj)):
        if i == user:
            listPrint(line2, i)
            d = line2.find('a', href=True)
            e = str(d['href'])
            sep = '/'
            href = e.split(sep, 1)[0]
            verbose("href:")
            verbose(href)
            return href

def showmedia(string):
    soup2 = req(string)
    getT1 = soup2.find_all('tr', attrs={'class':'lecture'})
    if (str(getT1)) != "[]":
        objPrint(getT1)
        userSelect = input("Select a lecture based on ID: ")
        user = int(userSelect)
        href = objPrintSelect(getT1, user)

        while True:
            userSelect2 = str(input("(a)udio, (c)amera, (s)creen or c(o)mbined? "))
            verbose("User input: ")
            verbose(userSelect2)
            if userSelect2 == 'a':
                link = "/audio.mp3"
                break
            elif userSelect2 == 'c':
                link = "/camera.mp4"
                break
            elif userSelect2 == 's':
                link = "/screen.mp4"
                break
            elif userSelect2 == 'o':
                link = "/combined.mp4"
                break
            else: print("\nType either 'a' for audio, 'c' for camera, 's' for screen or 'o' for combined\n")

        finish = URLPUBLISH + href + link
        subprocess.call([VLCPATH, finish])
        verbose("URL used: ")
        verbose(finish)

    else: print("\nNo results, did you misspell something?")

def searchfunction(topicORlecturer, SEARCHTL):
    soup = req(URLPUBLISH)
    list = soup.find_all('select', attrs={'name':topicORlecturer})
    for nr in list:
        hei = str(nr.text)
        for line in hei.splitlines():
            if SEARCHTL in line:
                print(line)

def listfunction(topicORlecturer):
    soup = req(URLPUBLISH)
    topicList = soup.find_all('select', attrs={'name':topicORlecturer})
    for topicList in topicList:
        print(topicList.text)

verbose("Verbose is enabled")

if STREAM:
    if STREAM in ROOMS:
        vlcstring = "http://forelesning.gjovik.ntnu.no/live/{}.html".format(STREAM)
        verbose("URL of stream:")
        verbose(vlcstring)
        subprocess.call([VLCPATH, vlcstring])
    else:
        print(STREAM)
        print("Is not a valid room. Use -h to view rooms")

if LIST_LECTURER:
    listfunction("lecturer")

if LIST_TOPIC:
    listfunction("topic")

if SEARCH_LECTURER:
    searchfunction("lecturer", SEARCH_LECTURER)


if SEARCH_TOPIC:
    searchfunction("topic", SEARCH_TOPIC)


if SELECT_LECTURER and SELECT_TOPIC:
    string = "http://forelesning.gjovik.ntnu.no/publish/index.php?lecturer={}&topic={}".format(SELECT_LECTURER, SELECT_TOPIC)
    string = string.replace(" ", "+")
    showmedia(string)


elif SELECT_LECTURER and not SELECT_TOPIC:
    string = "http://forelesning.gjovik.ntnu.no/publish/index.php?lecturer={}&topic=all".format(SELECT_LECTURER)
    string = string.replace(" ", "+")
    showmedia(string)

elif SELECT_TOPIC and not SELECT_LECTURER:
    string = "http://forelesning.gjovik.ntnu.no/publish/index.php?lecturer=all&topic={}".format(SELECT_TOPIC)
    string = string.replace(" ", "+")
    showmedia(string)
